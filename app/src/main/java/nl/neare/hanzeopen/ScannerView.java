package nl.neare.hanzeopen;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class ScannerView extends AppCompatActivity implements SurfaceHolder.Callback{
    Camera mCamera;
    SurfaceView mPreview;
    byte[] mBuffer;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {Manifest.permission.CAMERA}, 1);
            }
        }
        mPreview = (SurfaceView)findViewById(R.id.preview);
        mPreview.getHolder().addCallback(this);
        mPreview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mCamera = Camera.open();
        mCamera.setDisplayOrientation(90);
    }

    @Override
    public void onPause() {
        super.onPause();
        mCamera.stopPreview();
    }
    public byte[] getBuffer()
    {
        return mBuffer;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCamera.release();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        final Camera.Parameters params = mCamera.getParameters();
        List<Camera.Size> sizes = params.getSupportedPreviewSizes();

        Camera.Size selected = sizes.get(0);
        params.setPreviewSize(selected.width,selected.height);
        mCamera.setParameters(params);
        //mCamera.addCallbackBuffer(mBuffer);
        //setPreviewInterceptor();
        params.setPreviewFormat(ImageFormat.NV21);
        final Camera.Size size = getPictureSize();
        final Scanner scanner = new Scanner(this);
        scanner.setSize(size.width, size.height);

        mCamera.setPreviewCallback(new Camera.PreviewCallback() {
            boolean scanning = false;
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onPreviewFrame(byte[] data, Camera camera) {
                processFrame(data, camera);
            }
            private synchronized void processFrame(byte[] data, Camera camera)
            {
                YuvImage yuv = new YuvImage(data, params.getPreviewFormat(), size.width, size.height, null);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                //maybe pass the out to the callbacks and do each compression there?
                yuv.compressToJpeg(new Rect(0, 0, size.width, size.height), 50, out);

                byte[] bytes = out.toByteArray();
                Bitmap image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                scanner.setBitmap(image);
                scanner.blurImage();
                boolean match = scanner.edgeDetect();
                if(match) {
                    Intent i = new Intent(getBaseContext(), ImagePreview.class);
                    startActivity(i);
                    finish();
                }
            }
        });
        mCamera.startPreview();

    }

    private Camera.Size getPictureSize()
    {
        Camera.Parameters params = mCamera.getParameters();
        List<Camera.Size> sizes = params.getSupportedPreviewSizes();
        final int screenWidth = ((View) mPreview.getParent()).getWidth();
        Camera.Size bestSize = null;
        int minDiff = Integer.MAX_VALUE;
        for (Camera.Size size : sizes) {
            final int diff = Math.abs(size.height - screenWidth);
            if (Math.abs(size.height - screenWidth) < minDiff) {
                minDiff = diff;
                bestSize = size;
            }
        }
        return bestSize;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(mPreview.getHolder());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i("PREVIEW","surfaceDestroyed");
    }
}
