package nl.neare.hanzeopen;

import android.graphics.Bitmap;

/**
 * Created by woute on 2-11-2017.
 */

public class BlurRunner implements Runnable{

    Bitmap bm;
    int blur;
    int start_y;
    int end_y;
    int width;
    boolean finished = false;

    public BlurRunner(Bitmap parent, int blur, int start_y, int end_y, int width)
    {
        bm = parent;
        this.blur = blur;
        this.start_y = start_y;
        this.end_y = end_y;
        this.width = width;
    }
    public boolean getFinished()
    {
        return finished;
    }
    @Override
    public void run() {
//        for(int x = 0; x < width; x+= blur)
//        {
//            for(int y = start_y; y < end_y; y+= blur)
//            {
//                int r = 0;
//                int g = 0;
//                int b = 0;
//                int counter = 0;
//                for(int i = 0; i < blur; i++) {
//                    for (int j = 0; j < blur; j++) {
//                        int pixel = bm.getPixel(x + i, y + j);
//                        byte[] rgb = intToByteArray(pixel);
//                        r += rgb[1] & 0xFF;
//                        g += rgb[2] & 0xFF;
//                        b += rgb[3] & 0xFF;
//                        counter++;
//                    }
//                }
//                r = r / counter;
//                g = g / counter;
//                b = b / counter;
//                for(int i = 0; i < blur; i++) {
//                    for (int j = 0; j < blur; j++) {
//                        int colour = ((0xFF) << 24) | ((0xFF & r) << 16) |
//                                ((0xFF & g) << 8) | (0xFF & b);
//                        bm.setPixel(x+i,y+j, colour);
//                    }
//                }
//            }
//        }
        finished = true;
    }
    public static final byte[] intToByteArray(int value) {
        return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)value};
    }

}
