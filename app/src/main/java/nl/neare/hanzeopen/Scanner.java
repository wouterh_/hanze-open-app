package nl.neare.hanzeopen;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

/**
 * Created by woute on 2-11-2017.
 */

class Scanner{
    Bitmap bitmap;
    Bitmap copy;
    private int[][] pixelmap;
    private int[][] edgemap;
    private int done_threads = 0;
    private final int blurgroup = 10;
    private final int interlace = 20;
    private final int colourmatch = 30 * 3; // * 3 because 3 colours
    private int[][] olstmap = new int[160][90];
    private int width;
    private int height;
    private String olstbinary;
    private ScannerView parent;

    public void setSize(int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public Scanner(ScannerView parent){
        this.olstbinary = Olstbinary.binary;
        char[] cha = this.olstbinary.toCharArray();
        int i = 0;
        int j = 0;
        System.out.println(cha.length);

        for(char c: cha)
        {
            int fill = 1;
            if(Character.compare(c,'0') == 0)
            {
                fill = 0;
            }

            olstmap[i][j] = fill;
            j++;
            if(j == 90){
                i++;
                j = 0;
            }
        }
    }
    public void setBitmap(Bitmap bm)
    {
        pixelmap = new int[bm.getWidth()][bm.getHeight()];
        int[] temp_pixels = new int[bm.getWidth() * bm.getHeight()];
        bm.getPixels(temp_pixels,0,bm.getWidth(),0,0,bm.getWidth(),bm.getHeight());

        for(int i = 0; i < bm.getWidth(); i++)
        {
            for(int j = 0; j < bm.getHeight(); j++)
            {
                 pixelmap[i][j] = temp_pixels[i+j];
            }
        }
    }
    public synchronized void incrementDone()
    {
        done_threads++;
    }
    public synchronized int getDone()
    {
        return done_threads;
    }
    public void blurImage()
    {
        for(int x = 0; x < width; x+= blurgroup)
        {
            for(int y = 0; y < height; y+= blurgroup)
            {
                int r = 0;
                int g = 0;
                int b = 0;
                int counter = 0;
                for(int i = 0; i < blurgroup; i++) {
                    for (int j = 0; j < blurgroup; j++) {
                        int pixel = pixelmap[x+i][y+j];
                        byte[] rgb = intToByteArray(pixel);
                        r += rgb[1] & 0xFF;
                        g += rgb[2] & 0xFF;
                        b += rgb[3] & 0xFF;
                        counter++;
                    }
                }
                r = r / counter;
                g = g / counter;
                b = b / counter;
                for(int i = 0; i < blurgroup; i++) {
                    for (int j = 0; j < blurgroup; j++) {
                        int colour = ((0xFF) << 24) | ((0xFF & r) << 16) |
                                ((0xFF & g) << 8) | (0xFF & b);
                        pixelmap[x+i][y+i] = colour;
                    }
                }
            }
        }
    }

    public boolean comparePixels(int pixel_a, int pixel_b)
    {
        byte[] a = intToByteArray(pixel_a);
        a[1] &= 0xFF;
        a[2] &= 0xFF;
        a[3] &= 0xFF;
        byte[] b = intToByteArray(pixel_b);
        b[1] &= 0xFF;
        b[2] &= 0xFF;
        b[3] &= 0xFF;
        if(Math.abs((a[1]+a[2]+a[3])-(b[1]+b[2]+b[3])) >= colourmatch)
        {
            return true;
        }
        return false;
    }
    public boolean edgeDetect()
    {
        edgemap = new int[width][height];

        ArrayList<EdgeRunner> list = new ArrayList<>();

        for(int i = 0; i < olstmap.length; i++)
        {
            for(int j = 0; j < olstmap[0].length; j++)
            {
                if(olstmap[i][j] == 1){
                    EdgeRunner r = new EdgeRunner(this,
                            pixelmap,
                            edgemap,
                            height/olstmap[0].length*j,
                            height/olstmap[0].length*(j+1),
                            width/olstmap.length*i,
                            width/olstmap.length*(i+1),
                            width,
                            height);
                    list.add(r);
                    new Thread(r).start();
                }
            }
        }
        int length = list.size();
        System.out.println(length);
        while(getDone() < length)
        {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int count_edge = 0;
        for(EdgeRunner er: list)
        {
            count_edge += (er.found_edge) ? 1 : 0;
        }
        return (((double) count_edge / length) > 0.2) ? true: false;
    }

    public static final byte[] intToByteArray(int value) {
        return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)value};
    }
}
