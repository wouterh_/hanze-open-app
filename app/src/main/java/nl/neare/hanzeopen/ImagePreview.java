package nl.neare.hanzeopen;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebChromeClient;
import android.widget.ImageView;

public class ImagePreview extends AppCompatActivity {
    static Bitmap bm;
    protected static void setBitmap(Bitmap bm){
        ImagePreview.bm = bm;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagepreview);
        WebView wv = (WebView)findViewById(R.id.webview01);
        wv.setWebChromeClient(new WebChromeClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
                    view.getContext().startActivity(
                            new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
                    return false;
                }
            }
        });
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("https://hanze-open.nl/?open3d");
    }

}
