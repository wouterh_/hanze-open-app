package nl.neare.hanzeopen;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebChromeClient;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {Manifest.permission.INTERNET}, 1);
            }
        }
        setContentView(R.layout.activity_home);
        WebView wv = (WebView)findViewById(R.id.webview01);
        wv.setWebChromeClient(new WebChromeClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
                    view.getContext().startActivity(
                            new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
                    return false;
                }
            }
        });
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("https://maps.google.web-weave.com/");
    }

    protected void onClickFunction(View view)
    {
        System.out.println("Test!!!");

        Intent i = new Intent(getBaseContext(), ScannerView.class);
        startActivity(i);

    }
}
