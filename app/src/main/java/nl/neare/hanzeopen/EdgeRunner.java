package nl.neare.hanzeopen;

/**
 * Created by woute on 2-11-2017.
 */

public class EdgeRunner implements Runnable {
    int blur;
    int start_y;
    int end_y;
    int start_x;
    int end_x;
    boolean found_edge = false;
    int width;
    int height;
    int[][] pixelmap;
    int[][] edgemap;

    Scanner client;

    public EdgeRunner(Scanner parent_obj, int[][] parent, int[][] target, int start_y, int end_y, int start_x, int end_x, int width, int height)
    {
        pixelmap = parent;
        edgemap = target;
        client = parent_obj;
        this.start_y = start_y;
        this.end_y = end_y;
        this.width = width;
        this.height = height;
        this.start_x = start_x;
        this.end_x = end_x;
    }
    @Override
    public void run() {
        for(int y = start_y; y < end_y; y++)
        {
            for(int x = start_x; x < end_x; x++)
            {
                int pixel = 0;
                try {
                    pixel = pixelmap[x][y];
                }
                catch(ArrayIndexOutOfBoundsException e)
                {
                    System.out.println("START" + start_y);
                    System.out.println("END" + end_y);
                }
                // Left
                if(x > 0){
                    int left = pixelmap[x-1][y];
                    if(comparePixels(pixel, left))
                    {
                        found_edge = true;
                        client.incrementDone();
                        return;
                    }
                }
                // Right
                if(x < width-1)
                {
                    int right = pixelmap[x+1][y];
                    if(comparePixels(pixel, right))
                    {
                        found_edge = true;
                        client.incrementDone();
                        return;
                    }
                }
                // Up
                if(y > 0)
                {
                    int up = pixelmap[x][y-1];
                    if(comparePixels(pixel, up))
                    {
                        found_edge = true;
                        client.incrementDone();
                        return;
                    }
                }
                // Down
                if(y < height - 1)
                {
                    int down = pixelmap[x][y+1];
                    if(comparePixels(pixel, down))
                    {
                        found_edge = true;
                        client.incrementDone();
                        return;
                    }
                }
            }
        }
        client.incrementDone();
    }
    public static final byte[] intToByteArray(int value) {
        return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)value};
    }
    public boolean comparePixels(int pixel_a, int pixel_b)
    {
        byte[] a = intToByteArray(pixel_a);
        a[1] &= 0xFF;
        a[2] &= 0xFF;
        a[3] &= 0xFF;
        byte[] b = intToByteArray(pixel_b);
        b[1] &= 0xFF;
        b[2] &= 0xFF;
        b[3] &= 0xFF;
        if(Math.abs((a[1]+a[2]+a[3])-(b[1]+b[2]+b[3])) >= 30*3)
        {
            return true;
        }
        return false;
    }
}
